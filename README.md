# Minetest sub.nasa-g0v game

The vanilla anarchic famous server game

* English information: [GAMEINFO-en.md](GAMEINFO-en.md), **WARNING: if you question why there is no mod "x" instead of mod "y" read the information file in the initial section **
* Informacion español: [GAMEINFO-es.md](GAMEINFO-es.md), **ADVERTENCIA: si se cuestiona porque no esta el mod "x" en vez de el mod "y" lease el archivo de informacion en la seccion inicial**
* Информация русском: [GAMEINFO-ru.md](GAMEINFO-ru.md), **ВНИМАНИЕ: если у вас возник вопрос, почему нет мода "x" вместо мода "y" прочитайте информационный файл в начальном разделе**

![screenshot.png](screenshot.png)

For further information, check  https://codeberg.org/minenux/minetest-game-subnasa.git 
or https://git.minetest.io/minenux/minetest-game-subnasa

## Installation

For specific minetest installation check [docs/INSTALL.md](docs/INSTALL.md)

- Unzip the archive, rename the folder to `subnasa` and
place it in .. `minetest/games/`

- GNU/Linux: If you use a system-wide installation place
it in `~/.minetest/games/` or `/usr/share/games/minetest`.

## Compatibility

The Minetest subnasa Game GitHub master HEAD is compatible with MT 0.4.16, 4.0, 5.2 to 5.8 
that can be found at https://minetest.io but was only tested in 0.4.17 engine.

To download you can play this game with the following minetest engines:

* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-4.1
* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-4.0
* https://codeberg.org/minenux/minetest-engine-minetest/src/branch/stable-5.X
* Final minetest from https://minetest.io/downloads/ or https://minetest.org

This game is fully compatible with lasted 5.8 minetest, 2.0 multicraft and any final minetest versions

#### Mods

* minetest default and xtras
    * integrated the killme/game_commands were simplified into default mod, and provide CC-BY-SA-NC license
    * farming is default older but has build-in toolrank detection support
    * xdecor as `xdecor` [mods/xdecor](mods/xdecor) a super reduced version of homedecor pack, for performance
    * sunglasses into `default` from https://github.com/niwla23/minetest_game_improved/tree/master/mods/armoraddons
* sorceredkid auth mod
    * minetest Auth Redux as `auth_rx` [mods/auth_rx](mods/auth_rx) from https://codeberg.org/minenux/minetest-mod-auth_rx
    * so then minetest Formspecs as `formspecs` [mods/formspecs](mods/formspecs) from https://codeberg.org/minenux/minetest-mod-formspecs
* minetest floatlands as `floatlands` [mods/floatlands](mods/floatlands) derived from Floatlands Realm made to this servers, as https://codeberg.org/minenux/minetest-mod-floatland
    * so then default flowers as `flowers` are need. Later this will need ethereal modifications.
* minetest Random Spawn as `rspawn` [mods/rspawn](mods/rspawn) from https://codeberg.org/minenux/minetest-mod-rspawn
    * so then default beds as `beds` [mods/beds](mods/beds) from default 0.4
* tenplus1 petty mods but higly customized
    * mobs_redo as `mobs` [mods/mobs](mods/mobs) from https://codeberg.org/minenux/minetest-mod-mobs_redo
    * simple_skins as `skins` [mods/skins](mods/skins) from https://codeberg.org/minenux/minetest-mod-simple_skins
    * regrow as `regrow` [mods/regrow](mods/regrow) from https://codeberg.org/minenux/minetest-mod-regrow
    * ethereal as `ethereal` [mods/ethereal](mods/ethereal) from https://codeberg.org/minenux/minetest-mod-ethereal
* PilzAdam mobs mods and D00Med mobs
    * dmobs+animal+monsters as `mobs_jam` [mods/mobs_jam](mods/mobs_jam) from https://codeberg.org/minenux/minetest-mod-mobs_jam
* Balrog boss by STHGOM / sparky, Hamlet, BrandonReese, LordNeo
    * mobs_balrog as `mobs_jam` [mods/mobs_jam](mods/mobs_jam) from https://codeberg.org/minenux/minetest-mod-mobs_jam
* player stuffs:
    * minenux bags as `backpacks` [mods/backpacks](mods/backpacks) 
* NPXcoot
    * nsspf as `nsspf` [mods/nsspf](mods/nsspf) from https://git.minetest.io/minenux/minetest-mo>
* Shara RedCat ezhh
    * fireflies as `fireflies` [mods/fireflies](mods/fireflies) https://github.com/Ezhh/fireflies/blob/master/license.txt
* Skandarella
    * Wilhelmines Marinara as `marinara` [mods/marinara](mods/marinara) https://git.minetest.io/minenux/minetest-mod-marinara forked compatible version
* mckaygerhard
    * Governing dictatorchip as `governing` [mods/governing](mods/governing) https://gitlab.com/minenux/minetest-mod-governing

## Licensing

* (c) 2020 mckaygerhard
* (c) 2023 mckayshirou

CC-BY-SA-NC 4.0 (c) MckAY Lenz, See `LICENSE.txt`
