# installation documentation gameid subnasa

* English information: [INSTALL-en.md](INSTALL-en.md)
* Informacion español: [INSTALL-es.md](INSTALL-es.md)
* Информация русском: [INSTALL-ru.md](INSTALL-ru.md)

## Distribution specific install

Mostly under construction

### DEB based

* English Documentation for Debian and derivatives: 
* Documentacion Español para Debian y derivados: 
* Документация для Debian и производных: 

### RPM based

* English Documentation for Centos/Fedora/Suse/Redhat and derivatives: 
* Documentacion Español para Centos/Fedora/Suse/Redhat y derivados: 
* Документация для Centos/Fedora/Suse/Redhat и производных: 

### Alpine

* English Documentation for Alpine bare metal: 
* Documentacion Español para Alpine normal: 
* Документация для Alpine Linux: 

