# бесконечный манифест игры subnasa

Игра subnasa - самая быстрая под-игра без задержек в minetest!

## Информация

**Единственный враждебный элемент — это сами игроки**, а острые ощущения — **командная работа**.

Субназа имеет **приоритет, что многие входят и что существует опасность вмешательства**,
для этого хитрость заключается в том, чтобы обеспечить **простую среду без враждебных сущностей, легкий доступ**,
это привлечет много неопытных игроков, которые заполнят сервер, это позволит
что перед лицом опасности вмешательства **других игроков** в ваши вещи **вы должны жить группами**,
а также **спрятаться**, что заставит всех задуматься о том, как защитить себя, что невозможно сделать в одиночку..

Эта предпосылка означает, что такие моды, как `mobs_monster` или `dmobs`, не существуют, но если `mobs_animals`,
что означает, например, что нет брони или особых биомов, но много домов и украшений,
Кроме того, по причинам оптимизации такие вещи, как `unified_inventory`, не включены, но
если включены такие вещи, как `prestibags` и `ambience`, потому что игра должна загружаться на телефоны,
Память телефона иногда невелика, и такие моды, как `nssm`, требуют обработки большого количества данных. 

## Введение
---------------

Целью этого файла является не определение точной механики или целей, а только основная цель,
игра - это опыт группового задания, непрерывной игры снова с другими .. но стратегическим образом.
Игроки могут воровать, брать и подавать вещи из и для других ... как мир арачи.

В будущих выпусках могут появиться команды.

#### Предпосылка

Сервер может или не может сбросить мир дыр ... только те, которые носят игроки, останутся.

## Основной игровой процесс
----------------

Игра должна поощрять следующие основные элементы игрового процесса:

#### перевозчики вещи

Игроки должны заботиться о своей жизни и иметь вторую резервную копию, потому что мир может
могут быть сброшены в любой момент, останется только тот игрок, который носит в своем инвентаре или сумке.

#### Расчеты

Следует поощрять игроков строить поселения и объединяться в группы, чтобы выжить.

#### Выживание

Выживание должно состоять из двух частей, первая из которых должна быть вездесущей, постоянно вырисовывающейся угрозой.
с которыми борются игроки. Это должен быть постоянный фактор, необходимый игрокам.
выжить, чтобы продвинуться по игре.

Второй - гораздо меньшая угроза повседневной жизни и необходимость
исходят из этого. Эта вторая угроза никогда не должна затмевать первую, хотя
пренебрежение им может привести к серьезным недостаткам.

#### Исследование

Игроки должны исследовать и уходить далеко, но находить способы быстрее перемещаться по карте лунок,
никогда не будут вознаграждены за исследование местности вокруг них, хотя
исследование должно быть трудным, когда вы находитесь ближе к зоне возрождения. Зона возрождения
довольно опасно. cos заполнен, и именно здесь игроки начинают игру, и появляются читеры ламмеров.

## Прогресс игрока
---------------------

Прогресс только сам ... когда вы получаете более важные вещи и способы хранить, спрятать
и возродиться от их падения и иметь резервную копию материала. Потому что другие игроки всегда будут искать ресурсы.

Сложность должна возрастать со временем, опасность возрастает, когда присоединяется больше игроков, потому что
будет доступно меньше ресурсов, и придет большинство потенциальных мошенников.

## Конечные цели

Конечная цель игры - выжить и в конечном итоге избежать читеров.
или обычные игроки избили вас или украли ваши вещи, когда вы строите и делитесь с друзьями
