# minetest mod Backpacks
========================

Backpacks allow you to clean up space in your inventory.

Information
-----------

This mod must be named `backpacks` it featured to player a 
bag with shoulder straps that allow it to be carried on someone's back, 
or just simple bags to carried payer items. **This mod is a 
simplified version of one color and ones utility**

![screenshot.png](screenshot.png)

Tech information
----------------

Adds backpacks made of wool and leather, that can **store anything but themselves** 
in an 16 slots inventory. The backpacks can only be accessed once they're placed. 
Picking them up preserves their contents. They obey regular protection rules.

**This mod is a reduces simplified version of minenux one** the 
original one started as a unification effors from original 
https://codeberg.org/minenux/minetest-mod-backpacks

Unless all of those mods, this one is more simple and faster, and works with any version 
of the engine since 0.4 version.

#### Dependencies

* default
* wool
* dye

Optional dependences:

* mobs

#### Nodes Tools

They can be crafted by placing 8 wools or lather in a square around an empty craft grid centre.

| Node name                  | Description name      |
| -------------------------- | --------------------- |
| backpacks:backpack_wool    | Wool Backpack |
| backpacks:backpack_leather | Leather Backpack |

## LICENSE

Textures were provided originaly by James Stevenson but there is no hints about

Code were started by James Stevenson and later continue by Tai Kedzierski https://github.com/taikedz

License code is GPL3+ check [LICENSE.txt](LICENSE.txt) file.
