#    When true character model will appear in formspec with current skin as texture
skins_preview (Simple Skins Preview) bool true

#    Skin limit number to limit the number of skins read onto list
skins_limit (Skin Limit) int 300
