-- mod governor by mckaygerhard, geoip specific code
-- Copyright 2023

----------------------------------------------------------------------------
-- this program can be used free but cannot be used commertially or 
-- modified for, licenced CC-BY-SA-NC 4.0 unless explicit permission

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

----------------------------------------------------------------------------


--[[ init of call back on join of a player, ]]
minetest.register_on_joinplayer(function(player)

	local msgsys = "#anticheat cannot get player info from minetest api.."

	if not player then return end
	if not gapi.is_player(player) then return end
	if not minetest.get_player_ip then minetest.log("error", msgsys) end

	local ip
	local pos
	local name
	local info
	
	pos = player:getpos()
	name = player:get_player_name()
	ip = minetest.get_player_ip(name)
	info = minetest.get_player_information(name)

	-- natural checks for player object apar of player:is_player

	if not ip then 
		minetest.log("error", msgsys.." (ip) for "..name)
	end

	if not name then
		minetest.log("error", msgsys.." (name) seems fake player")
	end

	if not info then
		minetest.log("error", msgsys.." (info), processing limited")
	end

	-- process for ip information from player network

	if governing.checkapikey ~= "" then
		governing.checkip(ip, function(data) -- log, but TODO: record in storage log file
			local txt = gapi.format_result_checkip(data)
			if txt then minetest.log("warning", "[goberning/geoip] joined player, " .. name .. ":" .. ip .. "," .. txt)
			else minetest.log("error", "[goberning/geoip] joined player, " .. name .. ":" .. ip .. ", seems fails")
			end
			governing.joinplayer_callback(name, data) -- execute callback
		end)
	else
		if not governing.modgeoip then
			governing.lookup(ip, function(data) -- log, but TODO: record in storage log file
				local txt = gapi.format_result_checkip(data)
				if txt then minetest.log("warning", "[goberning/geoip] joined player, " .. name .. ":" .. ip .. "," .. txt)
				else minetest.log("error", "[goberning/geoip] joined player, " .. name .. ":" .. ip .. ", seems fails")
				end
				governing.joinplayer_callback(name, data) -- execute callback
			end)
		end
	end

	-- process for client and players information for cheated program

	if not info then
		if not gapi.is_player(player) then 
			name = name .. " is a bot confirmed, also"
		end
		msgsys = "[governing] can't retrieve info, player '" .. name .. "' disappeared or seems craker"
		minetest.log("warning", msgsys)
	else
		info = minetest.get_player_information(name)
		msgsys = "[governing] player '" .. name .. "' joined" ..
			" from:" .. info.address .. 
			" protocol_version:" .. (info.protocol_version or "20?") ..
			" formspec_version:" .. (info.formspec_version or "old") ..
			" lang_code:" .. (info.lang_code or "<unknown>") ..
			" serialization_version:" .. (info.serialization_version or "20?") ..
			" version_string:" .. (info.version_string or "not supported")
		minetest.log("action", msgsys)
	end

	if info.version_string then
		msgsys = "#anticheat detected a cheater client does not provide info, ".. tostring(ip).." named "..name
		local dfv = gapi.isdf(info.version_string)
		if dfv then
			minetest.after(3, function()
				minetest.chat_send_player(name, msgiplevelone);
				if minetest.settings:get_bool("beowulf.dfdetect.enable_kick", false) then
					minetest.kick_player(name, "Are you a cheater stupid user? change your cracked client for play")
				end
			end)
			minetest.log(msgsys)
		end
	else
		minetest.log(msgsys)
	end

end)
--[[ end of call back on join of a player, ]]


