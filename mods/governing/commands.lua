-- mod governor by mckaygerhard - commands module
-- Copyright 2020

----------------------------------------------------------------------------
-- this program can be used free but cannot be used commertially or 
-- modified for, licenced CC-BY-SA-NC 4.0 unless explicit permission

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

----------------------------------------------------------------------------





--[[ commands for killme implementation no matter of those mods ]]

if not governing.modkillme and not governing.modcommand then
	minetest.register_chatcommand("killme", {
		description = "Kill player or yourselft to respawn",
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if player then
				if minetest.settings:get_bool("enable_damage") and player:is_player() then
					player:set_hp(0)
					return true
				else
					for _, callback in pairs(minetest.registered_on_respawnplayers) do
						if callback(player) then
							return true
						end
					end
					-- There doesn't seem to be a way to get a default spawn pos from the lua API
					return false, "No static_spawnpoint defined"
				end
			else
				-- Show error message if used when not logged in, eg: from IRC mod
				return false, "You need to be online to be killed!"
			end
		end
	})
end

--[[ end of commands for killme implementation no matter of those mods ]]






-- -----------------------------------------------------------------------------------------

--[[ commands for geoip and ip location information ]]

if not governing.modgeoip then
	minetest.register_chatcommand("geoip", {
		params = "<playername>",
		privs = {geoip=true},
		description = "Does a geoip lookup on the given player for governing",
		func = function(name, param)

			if not param then return true, "usage: /geoip <playername>" end
			if not minetest.get_player_ip then return true, "[geoip]: engine its too older, canot get ip of player." end

			local ip = minetest.get_player_ip(param)
			if not ip then return true, "[geoip] no ip available! seems "..param.." does not provide such info!" end

			governing.lookup(ip, function(result)
				local txt = format_result_geoip(result)
				if not txt then return true, "[geoip]: "..param..":"..ip" error: "..(result.description or "unknown error") end
				minetest.log("action", "[geoip] "..param..": "..txt)
				minetest.chat_send_player(name,  param..": "..txt)
			end)

		end
	})
end

minetest.register_chatcommand("govip", {
	params = "<playername>",
	privs = {governing=true},
	description = "Governing geoip lookup + net detection details on the given player",
	func = function(name, param)
			if not param then return true, "usage: /govip <playername>" end
		if not minetest.get_player_ip then return true, "[governing]: engine its too older, canot get ip of player." end
			local ip = minetest.get_player_ip(param)
		if not ip then return true, "[governing] no ip available! seems "..param.." does not provide such info!" end
			governing.checkip(ip, function(result)
				local txt = format_result_checkip(result)
				if not txt then return true, "[governing]: "..param..":"..ip" error: "..(result.description or "unknown error") end
				minetest.log("action", "[governing] "..param..": "..txt)
				minetest.chat_send_player(name,  param..": "..txt)
			end)
		end
})

--[[ end of commands for geoip and ip location information ]]


--[[ commands for stamina or satiation based on existing mods ]]

if not governing.modhbhunger then
	if governing.modhudbars and not hb.redo then
		minetest.register_chatcommand("satiation", {
			privs = {["server"]=true},
			params = S("[<player>] <satiation>"),
			description = S("Set satiation of player or yourself"),
			func = function(name, param)
				if minetest.settings:get_bool("enable_damage") == false then
					return false, S("Not possible, damage is disabled.")
				end
				local targetname, satiation = string.match(param, "(%S+) (%S+)")
				if not targetname then
					satiation = param
				end
				satiation = tonumber(satiation)
				if not satiation then
					return false, S("Invalid satiation!")
				end
				if not targetname then
					targetname = name
				end
				local target = minetest.get_player_by_name(targetname)
				if target == nil then
					return false, S("Player @1 does not exist.", targetname)
				end
				if satiation > hbhunger.SAT_MAX then
					satiation = hbhunger.SAT_MAX
				elseif satiation < 0 then
				satiation = 0
				end
				hbhunger.hunger[targetname] = satiation
				hbhunger.set_hunger_raw(target)
				return true
			end,
		})
	end
end

--[[ end commands for stamina or satiation based on existing mods ]]


