minetest mod governing
======================

ADMIN mod, light version of various mods and extra tool for manage server

Information
-----------

@mckaygerhard PICCORO's improvement administration combo and commands for minetest

This mod attempts to be an improvement administration combo, but minimized 
as a light version of some other tools like geoip, names-per-ip, antigrif, antihack, 
etc etc etc

![](governing.png)

## Technical info
-----------------

This mod must be named `governing` and provides administrarion tools and 
procedures to manage server.

This mod features powerfully anticheat tools, mostly mixture of many others, 
and are able to detect the modified clients.

Repository : https://git.minetest.io/minenux/minetest-mod-governing

> **Note:** this mod requires that the server have compiled with extra information enabled

#### Configurations

| config param          | type   |  value     | req | default/min/mx  | example or description           |
| --------------------- | ------ | ---------- | --- | --------------- | -------------------------------- |
| secure.http_mods      | string | governing  | yes | none set        | geoip,governing                  |
| secure.trusted_mods   | string | governing  | yes | none set        | auth_rx,governing                |
| governing.checkapikey | string | `<string>` | yes | none set        | 177cd4797a4949c4b1114d2e4ab9af11 |
| anticheat.timestep    | int    | 15         | no  | 15 / 10 / 300   | How many time will run checks in seconds |
| anticheat.timeagain   | int    | 15         | no  | 15 / 10 / 300   | How many seconds checks again (to compare) on a suspected player |
| anticheat.moderators  | string | admin      | yes | admin,singleplayer | Comma separated list name players that can watch and check stats  |
| beowulf.dfdetect.enable_kick | boot | false | no  | false           | will kick the cheater when performs detections |

The `governing.checkapikey` string is requested by registering a free (or paid) 
account at https://vpnapi.io/ and **if not configured, will be used a simple geoip request**.

1. its powered by [VPNapi.io IP VPN & Proxy Detection API](https://vpnapi.io/api-documentation)
2. but if apykey is not set, its powered by [IP Location Finder by KeyCDN](https://tools.keycdn.com/geo)

> Warning: anticheat its a WIP from rnd's anticheat mod with beowulf mod fusioned

#### Commands

| command & format      | permission | description function   | observations            |
| --------------------- | --------- | ----------------------- | ----------------------- |
| `/killme`             | interact  | kill yourselft          | no matter if killme mod is present or not |
| `/govip <playername>` | governing | complete ip player info | Will require keyapi configuration set |
| `/geoip <playername>` | geoip     | simple ip player info   | no matter if geoip mod is present or not |
| `/cstats`             | moderator |  to see latest detected cheater | its not a privilegie, use config settings |
| `/cdebug`             | moderator | to see even suspected cheats to be verified later | its not a privilegie, use config settings |
| `/satiation`          | server    | manipulates the satiation of a player | only provide if our mod its present, the original mod already has it |
| `/cchk <playername>`  | moderator | checks for a cheater player | it send a msh rare to the cheater |

#### privs

* `geoip` can make a geoip query, no matter if geoip mod is present or missing
* `governing` can run administration command like ip check

> Warning: currently moderator its not a privs.. its a list of players names, its a WIP from rnd's anticheat mod with beowulf mod fusioned

#### Api

Currently only geoip functions are exposed, we later exposed antcheat also

```lua
-- lookup command
governing.checkip("213.152.186.35", function(result)
	-- see "Check ip information data"
end)

-- overrideable callback on player join
governing.joinplayer_callback = function(playername, result)
	-- see "Check ip information result data"
end
```

Check IP information on data:

```json
{
    "ip": "8.8.8.8",
    "security": {
        "vpn": false,
        "proxy": false,
        "tor": false,
        "relay": false,
    },
    "location": {
        "city": "",
        "region": "",
        "country": "United States",
        "continent": "North America",
        "region_code": "",
        "country_code": "US",
        "continent_code": "NA",
        "latitude": "37.7510",
        "longitude": "-97.8220",
        "time_zone": "America/Chicago",
        "locale_code": "en",
        "metro_code": "",
        "is_in_european_union": false
    },
    "network": {
        "network": "8.8.8.0/24",
        "autonomous_system_number": "AS15169",
        "autonomous_system_organization": "GOOGLE"
    }
}
```

## LICENSE

* Copyright (C) 2023 mckaygerhard <mckaygerhard@gmail.com> CC-BY-SA-NC 4.0
* Copyright (C) 2023 mckayshirou <mckayshirou@gmail.com> CC-BY-SA-NC 4.0
* Copyright 2016-2017 rnd  LGPL v3 for anticheat code of minetest 0.4 engines
* Copyright 2021-2023 BuckarooBanzay MIT for beowulf code of mt-mods

It applies  CC-BY-SA-NC 4.0 unless you ask to request permission
with special conditions. The ipcheck idea, the mod inclusion, 
can be distributed if the URL of repo are not altered when promotes.

URL : https://git.minetest.io/minenux and http://minetest.org

