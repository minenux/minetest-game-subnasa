-- mod governor by mckaygerhard
-- Copyright 2020

----------------------------------------------------------------------------
-- this program can be used free but cannot be used commertially or 
-- modified for, licenced CC-BY-SA-NC 4.0

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

----------------------------------------------------------------------------


--[[ init - initialization of variables ]]

local modname = "governing"  -- name of the mod , need to be used later in namespace mod variable
local modpath = minetest.get_modpath("governing")  -- path of the mod
local modstor = minetest.get_mod_storage() -- request of storage for the mod, if valid, otherwise false/nil
local worlddir = minetest.get_worldpath() -- path of the world were the mod is running, otherwise false/nil
local modmail = minetest.get_modpath("mail") -- path of the mail mod if available for sending messages internaly on private, otherwise false/nil
local modgeoip = minetest.get_modpath("geoip") -- if available, their commands wil be used, otherwise we provided
local moddefault = minetest.get_modpath("default") -- path of default mod if available, otherwise false/nil
local modkillme = minetest.get_modpath("killme") -- if killme is present as mod if available, otherwise we use own
local modcommand = minetest.get_modpath("game_commands") -- updated killme is present as mod if available, otherwise we use own
local modcreative = minetest.get_modpath("creative") -- if creative is available, otherwise false/nil
local modantispawn = minetest.get_modpath("antispam") -- if rnd1 anticheat mod is loaded, cos only wolrs with mt4
local modanticheat = minetest.get_modpath("anticheat") -- if rnd1 anticheat mod is loaded, cos only wolrs with mt4
local modbeowulf = minetest.get_modpath("beowulf") -- if rnd1 anticheat mod is loaded, cos only wolrs with mt4
local modhudbars = minetest.get_modpath("hudbars") -- if the hudbars mod is loaded and working, from wuzzy
local modhbhunger = minetest.get_modpath("hbhunger") -- if the hudbars hunger mod is loaded and working, from wuzzy
local checkapikey = minetest.settings:get("governing.checkapikey") or "" -- need for geoip improved ip information, then geoip normal will be used

local is_46 = minetest.has_feature("add_entity_with_staticdata") -- detect minetest engine 4.0.16 or mayor
local is_50 = minetest.has_feature("object_use_texture_alpha") -- detect mineitest engine more modern than 5.0.0
local is_53 = minetest.has_feature("object_step_has_moveresult") -- detect mineitest engine more modern than 5.3.1
local is_54 = minetest.has_feature("direct_velocity_on_players") -- detect mineitest engine more modern than 5.4.0

local httpapi = minetest.request_http_api and minetest.request_http_api() -- Only works at init time and must be called from the mod's main scope (not from a function).

local creative_mode_cache = minetest.settings:get_bool("creative_mode") -- detection if game is running in main creative

local S

--[[ end - all local variables ]]



--[[ init - translation boilerplace, currently some issues with intlib mod  ]]

if minetest.get_translator ~= nil then
	S = minetest.get_translator(modname) -- 5.x translation function
else
	if minetest.get_modpath("intllib") then
		dofile(minetest.get_modpath("intllib") .. "/init.lua")
		if intllib.make_gettext_pair then
			gettext, ngettext = intllib.make_gettext_pair() -- new gettext method
		else
			gettext = intllib.Getter() -- old text file method
		end
		S = gettext
	else -- boilerplate function
		S = function(str, ...)
			local args = {...}
			return str:gsub("@%d+", function(match)
					return args[tonumber(match:sub(2))]
				end)
			end
	end
end

--[[ end - translation boilerplace, currently some issues with intlib mod  ]]




--[[ init - namespace of the mod and varible usages ]]

gapi = {}
governing = {}

governing.is_46 = is_46 -- detect minetest engine 4.0.16 or mayor
governing.is_50 = is_50 -- detect mineitest engine more modern than 5.0.0
governing.is_53 = is_53 -- detect mineitest engine more modern than 5.3.1
governing.is_54 = is_54 -- detect mineitest engine more modern than 5.4.0

governing.S = S -- translations if available

governing.httpapi = httpapi -- must be called early at the beggining to work
governing.modname = modname -- name of the mod
governing.modpath = modpath -- path of the mod
governing.modstor = modstor -- request of storage for the mod, otherwise false/nil
governing.worlddir = worlddir -- path of the world were the mod is running, otherwise false/nil
governing.modgeoip = modgeoip -- path of the geoip mod if available for sending messages internaly on private, otherwise false/nil
governing.modmail = modmail -- path of the mail mod if available for sending messages internaly on private, otherwise false/nil
governing.moddefault = moddefault -- path of default mod if available, otherwise false/nil
governing.modkillme = modkillme -- if killme is present as mod if available, otherwise we use own
governing.modcommand = modcommand -- if same killme and commands is present as mod if available, otherwise we use own
governing.modantispawn = modantispawn -- autodetect if older mod antispan by appguru are present
governing.modanticheat = modanticheat -- if rnd1 anticheat mod is present cos only works with mt4
governing.modbeowulf = modbeowulf -- if beowulf anticheat mod is present cos only works with mt5
governing.modhudbars = modhudbars -- if hudbars is present or the mproved hudbars redo
governing.modhbhunger = modhbhunger -- if hbhunger is present or the mproved hudbars redo
governing.checkapikey = checkapikey -- need for geoip improved ip information, then geoip normal will be used

--[[ end - namespace of the mod and varible usages ]]



--[[ init http calls, must be only on main ]]

function governing.lookup(ip, callback)
	if not httpapi then
		minetest.log("error", "[governing/geoip] mod not in the trusted http mods!")
		return
	end
	httpapi.fetch( {
			url = "https://tools.keycdn.com/geo.json?host=" .. ip,
			extra_headers = { "User-Agent: keycdn-tools:https://minetest.org" },
			timeout = 5,
		}, function(res)
			if res.code == 200 then
				local data = minetest.parse_json(res.data)
				callback(data)
			else
				minetest.log("warning", "[governing/geoip] http request returned status: " .. res.code)
			end
		end)
end

function governing.checkip(ip, callback)
	if not httpapi then
		minetest.log("error", "[governing] mod not in the trusted http mods!")
		return
	end
	httpapi.fetch( {
			url = "https://vpnapi.io/api/" .. ip .. "?key==" .. governing.checkapikey,
			extra_headers = { "User-Agent: VenenuX-minenux:https://git.minetest.io/minenux" },
			timeout = 5,
		}, function(res)
			if res.code == 200 then
				local data = minetest.parse_json(res.data)
				callback(data)
			else
				minetest.log("warning", "[governing/ip] http request returned status: " .. res.code)
			end
		end)
end

--[[ end http calls, must be only on main ]]





--[[ include code files mod features ]]

dofile(governing.modpath.."/administration.lua")  -- more of commonformats

dofile(governing.modpath.."/anticheats.lua") -- load anticheat mod that combined rnd + beowulf + fairplay

dofile(governing.modpath.."/antispawn.lua") -- load antispawn chat features only if mod antispan is not loaded

dofile(governing.modpath.."/commands.lua") -- must be at end. so relies on others functionalities

dofile(governing.modpath.."/process.lua") -- must be at end. so relies on others functionalities

--[[ end of include code files mod features ]]




--[[ log files and finished of mod loading ]]

if governing.checkapikey == "" then minetest.log("warning", "[goberning] governing.checkapikey not set or empty, only basic geoip will work") end

print("[MOD] governing mod loaded" )

