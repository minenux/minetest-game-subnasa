Revision History
----------------------

Version 2.1b (30-Jun-2018)
  - initial beta version
  - included code samples for basic login filtering
  - included a command-line database import script

Version 2.2b (04-Jul-2018)
  - added install option to database import script
  - improved exception handling by AuthFilter class
  - fixed parsing of number literals in rulesets
  - fixed type-checking of try statements in rulesets
  - included mod.conf and description.txt files

Version 2.3b (08-Jul-2018)
  - general code cleanup of AuthFilter class
  - moved datasets into separate directory of world
  - added two more comparison operators for rulesets
  - tweaked pattern matching behavior in rulesets
  - changed database search method to use Lua regexes
  - removed hard-coded file names from database methods

Version 2.4b (13-Jul-2018)
  - moved Journal and AuthDatabase classes into library
  - added rollback function to AuthDatabase class
  - reworked journal audit to support rollback option
  - better encapsulated database commit function
  - allowed for STOPPED opcode during database update
  - various changes to error and action messages
  - moved command-line scripts to separate directory
  - included script to rollback database via journal
  - included script to extract debug log into journal

Version 2.5b (17-Jul-2018)
  - implemented function parsing algorithm for rulesets
  - simplified operand matching logic in rulesets
  - improved transcoding of literals in rulesets
  - added some basic functions for use by rulesets
  - fixed validation of dataset names in rulesets

Version 2.6b (19-Jul-2018)
  - introduced support for array literals in rulesets
  - added array-related functions for use by rulesets
  - localized references to transcoding functions
  - registered chat command to control login filtering
  - included support for disabling login filtering
  - added reload function to AuthFilter class
  - tweaked lexer to skip comments on ruleset loading
  - added search function to AuthDatabase class

Version 2.7b (22-Jul-2018)
  - implemented time and date datatypes for rulesets
  - updated code samples with latest feature-set
  - added time-related functions for use by rulesets
  - added time-related variables for use by rulesets
  - minor formatting fixes to source code
  - optimized comparison algorithm in ruleset parser

Version 2.8b (24-Jul-2018)
  - introduced support for numeric patterns in rulesets
  - created polymorphic pattern-matching classes
  - excluded time-zone offsets in time/date comparisons
  - standardized timestamps for use in filter functions
  - removed daylight saving time from preset variables
  - updated comparison algorithm in ruleset parser
  - passed preset variables array to filter functions

Version 2.9b (26-Jul-2018)
  - implemented address datatype for rulesets
  - added more helper functions for use by rulesets
  - fixed missing syntax checks in ruleset parser
  - developed and integrated AuthWatchdog class
  - added meta-variables for stateful login filtering

Version 2.10b (29-Jul-2018)
  - major code reorganization via multiple libraries
  - removed extra variables from AuthFilter class
  - developed interactive debugger for testing rulesets
  - added optional debugger hooks in AuthFilter class
  - allowed for overriding preset variables by debugger
  - included line-number in results of login filter
  - added missing preset variable needed by rulesets

Version 2.11 (04-Aug-2018)
  - developed a command-line player analytics script

Version 2.12 (10-Aug-2018)
  - better code consolidation of AuthFilter class
  - reworked ruleset parser to support syntax changes

Version 2.13 (20-Aug-2018)
  - developed in-game database management console
  - combined generic filter functions into superclass
  - updated debugger to use new GenericFilter class
  - added flag for constant-value operands in rulesets
  - simplified operand matching logic in rulesets
  - optimized comparison algorithm in ruleset parser
  - consolidated lookup tables of lexical analyzer
  - fixed erroneous status message shown in debugger
  - added support for per-player debugging sessions
  - redesigned login filter APIs for new architecture
  - switched order of return values in login filter
  - various code refactoring and better comments

Version 2.13.0 (20-Jul-2023)
  - nil player object check due problematic joins on incomplete auth process
  - added missing depends formspecs (it work without in basics but, some commands needs)
  - we will later aded formspecs checks to made optional
  - init the files when are fresh install, still do not convert from auth.txt if previous
  - improve the auth handler cos fails for recent versions of mineshit engine
  - fix deprecation warnings for fails handler of authentication player on recent engine
