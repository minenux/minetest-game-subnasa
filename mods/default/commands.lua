-- mod governor by mckaygerhard -- minimal subset of commands extracted
-- Copyright 2020

----------------------------------------------------------------------------
-- this program can be used free but cannot be used commertially or 
-- modified for, licenced only to CC BY-NC-SA 4.0

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

----------------------------------------------------------------------------

local killme_mod = minetest.get_modpath("killme")
local command_mod = minetest.get_modpath("game_commands")


if not killme_mod or not command_mod then
	minetest.register_chatcommand("killme", {
		description = "Kill yourself to respawn",
		func = function(name)
			local player = minetest.get_player_by_name(name)
			if player then
				if minetest.settings:get_bool("enable_damage") and player:is_player() then
					player:set_hp(0)
					return true
				else
					for _, callback in pairs(core.registered_on_respawnplayers) do
						if callback(player) then
							return true
						end
					end
					-- There doesn't seem to be a way to get a default spawn pos from the lua API
					return false, "No static_spawnpoint defined"
				end
			else
				-- Show error message if used when not logged in, eg: from IRC mod
				return false, "You need to be online to be killed!"
			end
		end
	})
end
