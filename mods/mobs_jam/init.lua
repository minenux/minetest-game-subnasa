local path = minetest.get_modpath("mobs_jam") .. "/"

-- Check for translation method
local S

if minetest.get_translator ~= nil then
	S = minetest.get_translator("mobs_jam") -- 5.x translation function
else
	if minetest.get_modpath("intllib") then
		dofile(minetest.get_modpath("intllib").."/init.lua")
		if intllib.make_gettext_pair then
			S = intllib.make_gettext_pair() -- new gettext method
		else
			S = intllib.Getter() -- old text file method
		end
	else -- boilerplate function
		S = function(str, ...)
			local args = {...}
			return str:gsub("@%d+", function(match)
				return args[tonumber(match:sub(2))]
			end)
		end
	end
end

local is50 = minetest.has_feature("object_use_texture_alpha") or nil
local is54 = minetest.has_feature("use_texture_alpha_string_modes") or nil

mobs.intllib_animal = S
mobs.is54a = is54

-- Check for custom mob spawn file
local input = io.open(path .. "spawn.lua", "r")

if input then
	mobs.custom_spawn_animal = true
	input:close()
	input = nil
end

-- helper function
local function ddoo(mob)

	if minetest.settings:get_bool("mobs_animal." .. mob) == false then
		print("[Mobs_Animal] " .. mob .. " disabled!")
		return
	end

	dofile(path .. mob .. ".lua")
end

if not minetest.get_modpath("mobs_animal") then
ddoo("chicken") -- JKmurray
ddoo("cow") -- KrupnoPavel
ddoo("rat") -- PilzAdam
ddoo("sheep") -- PilzAdam
ddoo("warthog") -- KrupnoPavel
ddoo("bee") -- KrupnoPavel
ddoo("bunny") -- ExeterDad
ddoo("kitten") -- Jordach/BFD
ddoo("penguin") -- D00Med
ddoo("panda") -- AspireMint
end

if not minetest.get_modpath("mobs_fish") and minetest.settings:get_bool("mobs_animal.fish") ~= false then
dofile(path .. "fishs.lua") -- mobs_water / mobs_fish
end

if not minetest.get_modpath("mobs_doomed") and not minetest.get_modpath("dmobs") then
dofile(path .. "fox.lua") -- D00Med
dofile(path .. "owl.lua") -- D00Med
dofile(path .. "tortoise.lua") -- D00Med
end

if not minetest.get_modpath("mobs_monster") then
dofile(path .. "mese_monster.lua") -- PilzAdam (WTFPL)
dofile(path .. "fire_spirit.lua") -- tenplus1
dofile(path .. "oerkki.lua") -- Pavel_S and PilzAdam (WTFPL)
dofile(path .. "lava_flan.lua") -- Lava Flan by Zeg9 (additional textures by JurajVajda)
end

if not minetest.get_modpath("mobs_balrog") then
dofile(path .. "balrog.lua") -- STHGOM / sparky and LordNeo
end

-- Load custom spawning
if mobs.custom_spawn_animal then
	dofile(path .. "spawn.lua")
end


-- Lucky Blocks
if minetest.get_modpath("lucky_block") then
	dofile(path .. "lucky_block.lua")
end


print ("[MOD] Mobs JAM (reduced animals mobs) loaded")
