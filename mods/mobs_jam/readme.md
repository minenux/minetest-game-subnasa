# minetest mod mobs_jam

Pack set like mobs, reduced, use if you dont want a buch of mods and only wants one that handles all

## Information

This mod is forked to improve compatibility and simplifycation, it combines/provide mobs from 
"animal", "dmobs" and "monsters" **simplified in one load to reduce calculations on server**.
Reason is the nascisic and petty behaviour of mayority of minetest developers

![](screenshot.png)

## Technical information

It provides mobs:

* `mobs_animal` a mixed from original PilzAdam with the ones of tenplus1 and 
    * `bee`
    * `chicken`
    * `cow`
    * `rat`
    * `sheep`
    * `panda`
    * `penguin`
    * `kitten`
    * `warthog`
* `mobs_water` disected few fish from animal mobpacks
    * `clowfish`
    * `tropical`
* `mobs_monster` disected from original PilzAdam, adapted to mobs_redo
    * `fire_spirit`
    * `oerkki`
    * `mese_monster`
* `mobs_doomed` or `dmobs` from D00Med aka heiselong lazy one
    * `fox`
    * `owl`
    * `tortoise`
* `mobs_balrog`
    * `balrog` !!

It also has backguard compatibility for 0.4 engine. It also provides some new translations and ajustments

**note** to gain performance equivalent compatibility with original mods are disabled by default, 
this mod provides by default aliasing of all items and mobs for backguard compatibility, you 
can enable it in settings

**warning** this provides aliasing only for entities that players interacts, not to the rest 
by example what does the lava flan shot as attack is not the same as what original lava flan shots, 
this is due breking compatibilty and easy mantain code rutines. If you need a mob you can use 
the same cos mapgen only mantain the mobs not the items that they shots.

**caution** this mobs try to fit all the items that player handles into their inventory, for 
compatibility, but you can diable it to gain performan by not registered so many items in engine.

### Depends

* mobs
* default
* farming
* fire
* tnt

Optionally

* nether
* intlib
* lucky_blocks

### Configurations

animals has a bunch of options

balrog needs `enable_tnt` to set to true if you want to explote on dead!

## Mobs:

**Note**: *After breeding, animals need to rest for 4 minutes and baby animals take 4 minutes to grow up, also feeding them helps them grow quicker...*

### Clowfish
Small fish on water

### Bee
Tends to buzz around flowers and gives honey when killed, you can also right-click a bee to pick it up and place in inventory. 3x bee's in a row can craft a beehive.

### Bunny
Bunnies appear in green grass areas (prairie biome in ethereal) and can be tamed with 4 carrots or grass. Can also be picked up and placed in inventory and gives 1 raw rabbit and 1 rabbit hide when killed.

### Chicken
Found in green areas (bamboo biome in ethereal) and lays eggs on flat ground, Can be picked up and placed in inventory and gives 1-2 raw chicken when killed. Feed 8x wheat seed to breed.

### Cow
Wanders around eating grass/wheat and can be right-clicked with empty bucket to get milk. Cows will defend themselves when hit and can be right-clicked with 8x wheat to tame and breed.

### Kitten
Found on green grass these cute cats walk around and can be picked up and placed in inventory as pets or right-clicked with 4x live rats or raw fish (found in ethereal) and tamed.  They can sometimes leave you little gifts of a hairball.

### Rat
Typically found around stone they can be picked up and cooked for eating.

### Sheep
Green grass and wheat munchers that can be clipped using shears to give 1-3 wool when tamed. Feed sheep 8x wheat to regrow wool, tame and breed.  Right-click a tamed sheep with dye to change it's colour.  Will drop 1-3 raw mutton when killed.

### Warthog
Warthogs unlike pigs defend themselves when hit and give 1-3 raw pork when killed, they can also be right-clicked with 8x apples to tame or breed.

### Penguin
These little guys can be found in glacier biomes on top of snow and have the ability to swim if they fall into water.

### Panda
These monochrome cuties spawn in Ethereal's bamboo biome and can be tamed with bamboo stalks :)  Remember they have claws though.

### tortoise
These were extracted from dmobs mod from D00Med and its friendly but pretty slowww

### Owl
These were extracted from dmobs mod and spawns at the leaves or trees, will rest at night and at day will hide

### Fox
These were extracted from dmobs mod, its aggressive and will not leave you from attack

### Lava Flan

Cute as they may look, lava flan wallow in their namesake (no, not flans) and get curious about players who wander by, forgetting 
that they can burn you and cause damage. They have a 1 in 5 chance of dropping lava orb when killed, but if they die in water then
 pray they dont solidify into an obsidian flan that shoots shards and destroys all around them.

### Oerkki

Found in dark areas like most monsters, Oerkki wander the caverns stealing away torches on the ground and attacking anyone found in that area. 1 in 3 chance of dropping obsidian.

### Fire Spirit

Fire Spirits will not tolerate players roaming around their domain and will fiercely attack until their dying puff of smoke. 
They will drop their spirit and some fire dust when using ethereal.

### Tropical fish

Big fish to catch on waters

### balrog

The final boss! it could inclusive crash the server.. be carefully!

#### Lucky Blocks: 20


## LICENSE

CC-BY-SA-NC unless permission of

* Copyright (C) 2022+ PICCORO Lenz MCKAY <mckaygerhard@mail.ru>

This mod handles several others works under following licences:

* Copyright (C) 2016-2020 tenplus1
* Copyright (C) 2015 Sapier
* Copyright (C) 2015 blert2112
* Copyright (C) 2015 LordNeo
* Copyright (C) 2014 Hamlet
* Copyright (C) 2014 STHGOM / sparky
* Copyright (C) 2014 BrandonReese
* Copyright (C) 2004-2012 Sam Hocevar <sam@hocevar.net>

Check license.txt for art and models
