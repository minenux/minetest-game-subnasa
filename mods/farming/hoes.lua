local use_toolranks = minetest.get_modpath("toolranks") or false
local use_extratoolranks = minetest.get_modpath("toolranks_extra") or false

local use_tr = (use_toolranks and (not use_extratoolranks)) or false

farming.register_hoe(":farming:hoe_wood", {
	description = "Wooden Hoe",
	inventory_image = "farming_tool_woodhoe.png",
	max_uses = 30,
	material = "group:wood",
	groups = {flammable = 2},
})
if use_tr then farming.add_hoe("farming:hoe_wood") end

farming.register_hoe(":farming:hoe_stone", {
	description = "Stone Hoe",
	inventory_image = "farming_tool_stonehoe.png",
	max_uses = 90,
	material = "group:stone"
})
if use_tr then farming.add_hoe("farming:hoe_stone") end

farming.register_hoe(":farming:hoe_steel", {
	description = "Steel Hoe",
	inventory_image = "farming_tool_steelhoe.png",
	max_uses = 200,
	material = "default:steel_ingot"
})
if use_tr then farming.add_hoe("farming:hoe_steel") end

farming.register_hoe(":farming:hoe_bronze", {
	description = "Bronze Hoe",
	inventory_image = "farming_tool_bronzehoe.png",
	max_uses = 220,
	material = "default:bronze_ingot"
})
if use_tr then farming.add_hoe("farming:hoe_bronze") end

farming.register_hoe(":farming:hoe_mese", {
	description = "Mese Hoe",
	inventory_image = "farming_tool_mesehoe.png",
	max_uses = 350,
	material = "default:mese_crystal"
})
if use_tr then farming.add_hoe("farming:hoe_mese") end

farming.register_hoe(":farming:hoe_diamond", {
	description = "Diamond Hoe",
	inventory_image = "farming_tool_diamondhoe.png",
	max_uses = 500,
	material = "default:diamond"
})
if use_tr then farming.add_hoe("farming:hoe_diamond") end
