-- Global farming namespace

farming = {}
farming.path = minetest.get_modpath("farming")


-- toolranks support
if minetest.get_modpath("toolranks") then
	function farming.add_hoe(material)
		-- registering as tool
		local name = material
		toolranks.add_tool(material)

		-- getting after_use
		local def = minetest.registered_items[name]
		local hoe_on_use = def.on_use
		local hoe_after_use = def.after_use

		if (hoe_on_use == nil) or (hoe_after_use == nil) then
			return
		end
		minetest.override_item(name, {
			-- we also want hoes to increase dugnodes when farming soil
			on_use = function(itemstack, user, pointed_thing, uses)
				-- if no node is pointed, the hoe cannot be used
				if pointed_thing.under == nil then
					return nil
				end
				local under = minetest.get_node(pointed_thing.under)
				-- get origin wear
				local wear = itemstack:get_wear()
				-- apply previous on_use
				local ret_itemstack = hoe_on_use(itemstack, user, pointed_thing, uses)
				if ret_itemstack == nil then
					return nil
				end
				-- compute wear diff
				local hoe_uses = ret_itemstack:get_wear() - wear
				-- set wear back because it is up to hoe_after_use to add wear
				ret_itemstack:set_wear(wear)
				-- apply afteruse
				return hoe_after_use(ret_itemstack, user, under, {wear = hoe_uses})
			end
		})
	end
end

-- Load files

dofile(farming.path .. "/api.lua")
dofile(farming.path .. "/nodes.lua")
dofile(farming.path .. "/hoes.lua")

-- WHEAT

farming.register_plant("farming:wheat", {
	description = "Wheat Seed",
	paramtype2 = "meshoptions",
	inventory_image = "farming_wheat_seed.png",
	steps = 8,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland"},
	groups = {flammable = 4},
	place_param2 = 3,
})

minetest.register_craftitem("farming:flour", {
	description = "Flour",
	inventory_image = "farming_flour.png",
	groups = {flammable = 1},
})

minetest.register_craftitem("farming:bread", {
	description = "Bread",
	inventory_image = "farming_bread.png",
	on_use = minetest.item_eat(5),
	groups = {flammable = 2},
})

minetest.register_craft({
	type = "shapeless",
	output = "farming:flour",
	recipe = {"farming:wheat", "farming:wheat", "farming:wheat", "farming:wheat"}
})

minetest.register_craft({
	type = "cooking",
	cooktime = 15,
	output = "farming:bread",
	recipe = "farming:flour"
})


-- Cotton

farming.register_plant("farming:cotton", {
	description = "Cotton Seed",
	inventory_image = "farming_cotton_seed.png",
	steps = 8,
	minlight = 13,
	maxlight = default.LIGHT_MAX,
	fertility = {"grassland", "desert"},
	groups = {flammable = 4},
})

minetest.register_craftitem("farming:string", {
	description = "String",
	inventory_image = "farming_string.png",
	groups = {flammable = 2},
})

minetest.register_craft({
	output = "wool:white",
	recipe = {
		{"farming:cotton", "farming:cotton"},
		{"farming:cotton", "farming:cotton"},
	}
})

minetest.register_craft({
	output = "farming:string 2",
	recipe = {
		{"farming:cotton"},
		{"farming:cotton"},
	}
})


-- Straw

minetest.register_craft({
	output = "farming:straw 3",
	recipe = {
		{"farming:wheat", "farming:wheat", "farming:wheat"},
		{"farming:wheat", "farming:wheat", "farming:wheat"},
		{"farming:wheat", "farming:wheat", "farming:wheat"},
	}
})

minetest.register_craft({
	output = "farming:wheat 3",
	recipe = {
		{"farming:straw"},
	}
})


-- Fuels

minetest.register_craft({
	type = "fuel",
	recipe = "farming:straw",
	burntime = 3,
})

minetest.register_craft({
	type = "fuel",
	recipe = "farming:wheat",
	burntime = 1,
})

minetest.register_craft({
	type = "fuel",
	recipe = "farming:cotton",
	burntime = 1,
})

minetest.register_craft({
	type = "fuel",
	recipe = "farming:string",
	burntime = 1,
})

minetest.register_craft({
	type = "fuel",
	recipe = "farming:hoe_wood",
	burntime = 5,
})
