-- minetest.clear_registered_biomes() -- do not regenerate, we want to combine it!

-- Get setting or default
local is_54 = minetest.has_feature("direct_velocity_on_players") or false
local mgv7 = minetest.get_mapgen_setting("mg_name") or nil
local mgv7_spflags = minetest.get_mapgen_setting("mgv7_spflags") or "mountains, ridges, floatlands, caverns"
local captures_float = string.match(mgv7_spflags, "floatlands")
local captures_nofloat = string.match(mgv7_spflags, "nofloatlands")
local floatland_y = minetest.get_mapgen_setting("mgv7_floatland_level") or 2280
local worldlimit = minetest.get_mapgen_setting("mapgen_limit") or 31000
local mount_height = minetest.get_mapgen_setting("mgv7_float_mount_height") or 256
local mount_dens = minetest.get_mapgen_setting("mgv7_float_mount_density") or 0.6

-- Make global for mods to use to register floatland biomes
default.mgv7_floatland_level = floatland_y
default.mgv7_shadow_limit = minetest.get_mapgen_setting("mgv7_shadow_limit") or 1024

-- Registering of the nodes
if mgv7 then
minetest.register_node("floatland:grass", {
	description = "High Grass",
	tiles = {"floatland_realm_grass.png", "floatland_realm_dirt.png",
		{name = "floatland_realm_dirt.png^floatland_realm_grass_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 2, soil = 1, spreading_dirt_type = 1},
	drop = 'floatland:dirt',
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("floatland:sand", {
	description = "High Sand",
	tiles = {"floatland_realm_sand.png"},
	drop = 'floatland:sand',
	groups = {crumbly = 2, falling_node = 1, sand = 1},
	sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("floatland:dirt", {
	description = "High Dirt",
	tiles = {"floatland_realm_dirt.png"},
	drop = 'default:dirt',
	groups = {crumbly = 2, soil = 1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("floatland:stone", {
	description = "High Stone",
	tiles = {"floatland_realm_stone.png"},
	groups = {cracky = 2, stone = 1},
	drop = 'floatland:stone',
	legacy_mineral = true,
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("floatland:rare_sand", {
	description = "High Rare Sand",
	tiles = {"floatland_rare_sand.png"},
	drop = "floatland:salt 6",
	groups = {crumbly = 3, soil = 1, spreading_dirt_type = 1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

minetest.register_node("floatland:sand_crystal_block", {
	description = "High Crystal Block",
	drawtype = "glasslike_framed_optional",
	tiles = {"floatland_sand_crystal_block.png"},
	paramtype = "light",
	paramtype2 = "glasslikeliquidlevel",
	param2 = 255,
	use_texture_alpha = (is_54 and "blend" or true),
	sunlight_propagates = false,
	groups = {cracky = 2},
	sounds = default.node_sound_glass_defaults(),
	walkable = true,
	drop = "floatland:sand_crystals 6"
})

-- Registering of items

minetest.register_craftitem("floatland:sand_crystals", {
    description = "High Sand Crystals",
    wield_image = "floatland_sand_crystals.png",
    inventory_image = "floatland_sand_crystals.png",
    groups = {salt= 1},
})

minetest.register_craftitem("floatland:salt", {
    description = "High Salt",
    wield_image = "floatland_salt.png",
    inventory_image = "floatland_salt.png",
    groups = {salt= 1},
})

minetest.register_craft({
	type = "shapeless",
	output = "floatland:salt",
	recipe = {"floatland:sand_crystals"},
})



-- Registering of the biomes

minetest.register_biome({
	name = "floatland_nicegrass",
	node_top = "floatland:grass",
	depth_top = 1,
	node_filler = "floatland:dirt",
	depth_filler = 2,
	node_riverbed = "floatland:rare_sand",
	depth_riverbed = 2,
	node_water = "default:water_source",
	node_river_water = "default:river_water",
	depth_water_top = 15,
	node_water_top = "default:water_source",
	node_stone = "floatland:stone",
	y_min = floatland_y + 40,
	y_max = worldlimit,
	heat_point = 50,
	humidity_point = 50,
})

minetest.register_biome({
	name = "floatland_nicebeach",
	node_top = "floatland:sand",
	depth_top = 3,
	node_filler = "floatland:sand_crystal_block",
	depth_filler = 3,
	node_riverbed = "floatland:rare_sand",
	depth_riverbed = 2,
	node_water = "default:water_source",
	node_river_water = "default:river_water",
	depth_water_top = 5,
	node_water_top = "default:water_source",
	node_stone = "floatland:stone",
	y_min = default.mgv7_shadow_limit,
	y_max = floatland_y + 40,
	heat_point = 50,
	humidity_point = 50,
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = {"floatland:grass"},
	sidelen = 16,
	fill_ratio = 0.5,
	biomes = {"floatland_nicegrass"},
	y_min = floatland_y + 40,
	y_max = worldlimit,
	decoration = "default:bush_sapling",
})
end
