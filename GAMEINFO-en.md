# infinite subnasa game play manifiesto

The subnasa game is the lag-free most faster subgame in minetest!

## Information

The **only hostile element is the players themselves** and the thrill is **teamwork**.

The subnasa has as a **priority that many enter and that there is a danger of interference**,
for this the trick is to provide an **easy environment and without hostile entities, easy to access**,
this will attract many inexperienced players which will fill the server, this will allow
that in the face of the danger of **other players interfering** with your things, **you have to live in groups**,
as well as **hide**, which will make everyone think about how to defend themselves, which cannot be done alone..

This premise means that mods like `mobs_monster` or `dmobs` do not exist, but if `mobs_animals`,
which means for example that there are no armor or special biomes, but many houses and decoration,
Additionally, for optimization reasons things like `unified_inventory` are not included but
if things like `prestibags` and `ambience` are included, because the game must load on phones,
phone memory is sometimes not large and mods like `nssm` involve a lot of data to deal with.

## Introduction
---------------

The purpose of this file is not to define exact mechanics or goals, only the main goal, 
the game is a experience of group task, continuous play again others.. but in strategic way.
Players may stole, take and serve stuff from and for others.. like a arachi one world.

Future releases may introduce teams.

#### Premise

Server may or may nor reset the hole world.. ony that players carries will remain.

## Core Gameplay
----------------

The game should encourage the following core gameplay elements:

#### carriers stuffs

Players must take care of their life and have second one backup, cos world can 
be reseted in any moment, only that player carries in their inventory or bag will remain.

#### Settlements

The players should be encouraged to build settlements, and group together to survive.

#### Survival

Survival should take two parts, first should be an ever-present, ever-looming threat
that players fight against. It should be the constant factor that players need to
survive against to progress through the game.

The second is the much smaller threat of basic day-to-day life, and the needs that
come from that. This second threat should never overshadow the first, although
neglecting it could result in a severe disadvantage against it.

#### Exploration

The players should explore and go far away but find ways to move on the hole map faster, 
never will be rewarded for exploring the area around them, although
exploration should be difficult when are more close to spawn zone. The spawn zone 
is pretty dangerous. cos is full and is where players start, and lammers cheaters appears.

## Player Progression
---------------------

The progression is only by itselft.. when gain more important stuff and ways to store, hide 
and revive from their fall and have back up of stuff. Cos other players always willchase resources.

Difficulty should ramp-up over time, danger is increased when more players are joined, cos 
less resources will are available and most potential cheaters come in.

## End Goals

The end goal of the game is to survive, and eventually avoid cheaters 
or normal players beat you, or stolen your stuffs, when you are building and sharing with your friends

