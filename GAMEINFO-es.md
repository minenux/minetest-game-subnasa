# juego subnasa infinito manifiesto

¡El juego subnasa es el subjuego más rápido sin lag en minetest!

## Informacion

El **unico elemento hostil son los jugadores mismos** y la emocion es el **trabajo en equipo**.

El subnasa tiene como **prioridad que muchos entren y que exista el peligro de interferencias**, 
para ello el truco es proveer un **entorno facil y sin entidades hostiles, facil de acceder**, 
esto atraera a muchos jugadores inexpertos lo que llenara el servidor, esto permitira 
que ante el peligro de que **otros jugadores interfieran** con tus cosas, **tengas que convivir en grupos**, 
asi como **esconderse**, lo que hara que entre todos piensen como defenderse lo cual no se puede solo..

Esta premisa hace que mods como `mobs_monster` o `dmobs`  no existan, pero si `mobs_animals`,
lo que significa por ejemplo que no hay armaduras ni biomas especiales, pero si muchas casas y decoracion, 
adicional, por razones de optimizacion no se incluyen cosas como `unified_inventory` pero 
si se incluyen cosas como `prestibags` y `ambience`, porque el juego debe cargar en telefonos, 
la memoria de los telefonos a veces no es grande y mods como `nssm` implican muchos datos que manejar.

## Introducción
---------------

El propósito de este archivo no es definir mecánicas u objetivos exactos, solo el objetivo principal,
el juego es una experiencia de tarea grupal, juego continuo otra vez otros .. pero de forma estratégica.
Los jugadores pueden robar, tomar y servir cosas de y para otros ... como un mundo arachi.

Los lanzamientos futuros pueden introducir equipos de trabajo.

#### Premisa

El servidor puede o no reiniciar el mundo del agujero ... solo los jugadores que llevan se mantendrán.

## Jugabilidad básica
----------------

El juego debe fomentar los siguientes elementos básicos del juego:

#### productos de los transportistas

Los jugadores deben cuidar su vida y tener un segundo respaldo, porque el mundo puede
reiniciarse en cualquier momento, solo quedará lo que el jugador lleve en su inventario o bolsa.

#### Liquidaciones

Se debe alentar a los jugadores a que construyan asentamientos y se agrupen para sobrevivir.

#### Supervivencia

La supervivencia debe tener dos partes, primero debe ser una amenaza omnipresente y omnipresente
contra los que luchan los jugadores. Debería ser el factor constante que los jugadores necesitan para
sobrevivir contra para progresar en el juego.

El segundo es la amenaza mucho menor de la vida diaria básica y las necesidades que
vienen de eso. Esta segunda amenaza nunca debería eclipsar a la primera, aunque
descuidarlo podría resultar en una seria desventaja contra él.

#### Exploración

Los jugadores deben explorar e irse lejos, pero encontrar formas de moverse más rápido en el mapa.
nunca será recompensado por explorar el área que los rodea, aunque
la exploración debería ser difícil cuando están más cerca de la zona de spawn. La zona de spawn 
es bastante peligroso. porque está lleno y es donde empiezan los jugadores, y aparecen lammers tramposos.

## Progresión del jugador
---------------------

La progresión es solo por sí misma ... cuando obtienes cosas más importantes y formas de almacenar, esconder
y revivir de su caída y tener una copia de seguridad de las cosas. Porque otros jugadores siempre buscarán recursos.

La dificultad debería aumentar con el tiempo, el peligro aumenta cuando se unen más jugadores, porque
habrá menos recursos disponibles y la mayoría de los posibles tramposos entrarán.

## Objetivos finales

El objetivo final del juego es sobrevivir y, finalmente, evitar a los tramposos.
o los jugadores normales te golpean, o te roban tus cosas, cuando estás construyendo y compartiendo con tus amigos 
